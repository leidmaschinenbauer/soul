# Leidmaschine :: Soul

Backend for the Leidmaschine installation:

* Face recognition & database of known identities
* Pain detection via button
* Compilation of pain infliction statistics
* Communication with frontend

## Requirements

* Python 3
* scipy and numpy
* OpenCV
* DLIB

## Setup

* Run `fetch-detectors.sh` to download necessary dlib detector files (needs `wget` and `bzip2`)
* Run `python soul.py`
