import cv2
import dlib
import numpy as np
from scipy.spatial.distance import cdist
import time
from datetime import datetime, timedelta
from uuid import uuid4
import pyglet
import os
import json
from urllib import request, parse
import threading

DEBUG = False

# Path to communication directory with frontend
SPINE = "{}/.local/share/love/Leidmaschine/events".format(os.getenv("HOME"))

# Remote Database
DB_URL = "https://leidmaschine.info/_db"
with open("db-key") as fin:
    DB_KEY = fin.read()

def serializeTime(fout, t):
    fout.write(t.strftime("%Y-%m-%d %H:%M:%S\n"))
def serializeNumber(fout, i):
    fout.write(str(i) + "\n")

class Identity(object):
    def __init__(self, uuid = None, seenFirst = None, seenBeforeLast = None, seenLast = None, valence = 0, feat = None):
        self.uuid = uuid if uuid is not None else str(uuid4())
        self.seen = dict(first = datetime.now(), beforeLast = datetime.now(),
                         last = datetime.now())
        if seenFirst is not None:
            self.seen["first"] = seenFirst
        if seenBeforeLast is not None:
            self.seen["beforeLast"] = seenBeforeLast
        if seenLast is not None:
            self.seen["last"] = seenLast
        self.valence = valence
        self.features = feat if feat is not None else np.array([]).reshape(-1,128)
        self.center = (0,0)

    def distances(self, x):
        self.dist = cdist(self.features, x, 'sqeuclidean')
        return self.dist

    def match(self, x):
        return (self.distances(x) < .36).any()

    def updateFace(self, x, center):
        if self.features.shape[0] == 0:
            self.features = x
        else:
            self.features = np.r_[self.features, x]
        if self.features.shape[0] > 20: # remove feature most similar to current
            sel = np.arange(self.features.shape[0]) != np.argmin(self.dist)
            self.features = self.features[sel]
        self.seen["beforeLast"], self.seen["last"] = self.seen["last"], datetime.now()
        self.center = center

    def save(self, path):
        with open("{}/{}.meta".format(path, self.uuid), "w") as fout:
            serializeTime(fout, self.seen["first"])
            serializeTime(fout, self.seen["beforeLast"])
            serializeTime(fout, self.seen["last"])
            serializeNumber(fout, self.valence)
        np.save("{}/{}.db.npy".format(path, self.uuid), self.features)

    @staticmethod
    def load(path, uuid):
        with open("{}/{}.meta".format(path, uuid), "r") as fin:
            cts = fin.read().split("\n")
            seenFirst = datetime.strptime(cts[0], "%Y-%m-%d %H:%M:%S")
            seenBeforeLast = datetime.strptime(cts[1], "%Y-%m-%d %H:%M:%S")
            seenLast = datetime.strptime(cts[2], "%Y-%m-%d %H:%M:%S")
            valence = float(cts[3])
            return Identity(uuid, seenFirst, seenBeforeLast, seenLast, valence,
                            np.load("{}/{}.db.npy".format(path,uuid)))

class Memory(object):
    def __init__(self, db = None):
        self.db = db if db is not None else []
        self.byID = {p.uuid: p for p in db}
        self.presses = []

    def find(self, x):
        for p in self.db:
            if p.match(x):
                return p

        # no match: append new identity and return
        p = Identity()
        self.db.append(p)
        self.byID[p.uuid] = p
        return p

    def get(self, id):
        return self.byID[id]

    def save(self, dt, path):
        print("Dumping db")
        try:
            os.makedirs(path)
        except FileExistsError:
            pass

        # purge old (not seen for 3 days) identities from filesystem and db
        now = datetime.now()
        for p in self.db:
            if now - p.seen["last"] < timedelta(3):
                p.save(path)

    @staticmethod
    def load(path):
        db = []
        now = datetime.now()
        for uuid,path in ((f[:-5],path) for f in os.listdir(path) if f.endswith('.meta')):
            p = Identity.load(path, uuid)
            if now - p.seen["last"] < timedelta(1):
                db.append(p)
            else:
                os.remove(os.path.join(path, "{}.meta").format(uuid))
                os.remove(os.path.join(path, "{}.db.npy").format(uuid))
        return Memory(db)

class Thread():
    def __init__(self, *args):
        self.detector = dlib.get_frontal_face_detector()
        self.sp = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
        self.facerec = dlib.face_recognition_model_v1("dlib_face_recognition_resnet_model_v1.dat")

        self.memory = Memory.load("db")

        self.cap = cv2.VideoCapture(0)

    def __enter__(self):
        # dump db every 30 minutes
        pyglet.clock.schedule_interval(self.memory.save, 30*60, "db")
        # update stats every 30 minutes
        pyglet.clock.schedule_interval(self.reportAbuse, 30*60)
        # grab frame every second
        pyglet.clock.schedule_interval(self.lookAround, 1)
        # poll button event every 100ms
        pyglet.clock.schedule_interval(self.feel, .1)
        # poll valence update event every 250ms
        pyglet.clock.schedule_interval(self.remember, .25)

        if DEBUG:
            self.win = dlib.image_window()

    def __exit__(self, type, value, traceback):
        self.cap.release()
        self.memory.save("db")
        self.reportAbuse()

    def sendEvent(self, type, **kwargs):
        msg = json.dumps({'type':type, **kwargs})
        with open("{}/det-{}.json".format(SPINE, uuid4()), "w") as f:
            f.write(msg)

    # send summary statistic to server
    def reportAbuse(self, dt):
        t = threading.Thread(target=self._dbUpdateThread, daemon=True)
        t.run()

    def _dbUpdateThread(self):
        print("Push DB update")
        try:
            res = json.loads(request.urlopen("{}/state".format(DB_URL)).read())
            dbstate = datetime(res["year"], res["month"], res["day"], res["hour"], res["minute"])
        except Exception as e:
            print("Cannot get state of db")

        delta = [{"date": p["date"].isoformat()[:16], "t": p["t"], "uuids": p["uuids"]}
                 for p in self.memory.presses if p["date"] >= dbstate]
        try:
            if len(delta) > 0:
                data = str.encode(parse.urlencode({'delta':json.dumps(delta),
                                                   'key':DB_KEY}))
                res = request.urlopen(DB_URL, data=data)
                self.memory.presses = []
            print("DB Update done")
        except Exception as e:
            print("cannot update db")

    # poll button release event
    def feel(self, dt):
        for f in (os.path.join(SPINE,f) for f in os.listdir(SPINE) if f[:3] == "btn"):
            with open(f) as fin:
                info = json.load(fin)
                info["date"] = datetime.now()
                self.memory.presses.append(info)
            os.remove(f)

    # poll valence update
    def remember(self, dt):
        DISCOUNT = .9 # XXX: change me?
        for f in (os.path.join(SPINE,f) for f in os.listdir(SPINE) if f[:3] == "val"):
            with open(f) as fin:
                info = json.load(fin)
                for uuid in info["uuids"]:
                    p = self.memory.get(uuid)
                    p.valence = p.valence * DISCOUNT + info["valence"] * (1 - DISCOUNT)
            os.remove(f)

    def lookAround(self, dt):
        # skip a few frames to make sure we get a fresh image
        for i in range(20):
            self.cap.grab()

        # grab frame and detect people
        ret, frame = self.cap.read()
        frame = frame[:,:,::-1] # bgr to rgb

        seenPeople = []
        dets = self.detector(frame, 0)
        for det in dets:
            center = det.center()
            center = (2.0 * center.x / frame.shape[1] - 1,
                      2.0 * center.y / frame.shape[0] - 1)
            shape = self.sp(frame, det)
            descr = np.array(self.facerec.compute_face_descriptor(frame, shape))
            descr = descr.reshape(1,-1)
            p = self.memory.find(descr)
            p.updateFace(descr, center)
            seenPeople.append(p)

        if DEBUG:
            self.win.clear_overlay()
            self.win.set_image(frame)
            self.win.add_overlay(dets)

        self.sendEvent("detection", detections=[{
                'uuid': p.uuid,
                'first-seen': p.seen["first"].strftime("%Y-%m-%d %H:%M:%S"),
                'last-seen': p.seen["beforeLast"].strftime("%Y-%m-%d %H:%M:%S"),
                'center': p.center,
                'valence': p.valence
            } for p in seenPeople])


with Thread():
    pyglet.app.run()
